import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  filesToUpload: Array<File> = [];
  message: any = [];
  myStyle: object = {};
  myParams: object = {};
  width: number = 50;
  height: number = 50;


  constructor(private http: HttpClient){}


  upload() {
    const formData: any = new FormData();
    const files: Array<File> = this.filesToUpload;
    console.log(files);

    for(let i =0; i < files.length; i++){
        formData.append("uploads[]", files[i], files[i]['name']);
    }
    console.log('form data variable :   '+ formData.toString());
    this.http.post('http://localhost:1234/upload', formData).subscribe(files => console.log('files', files),
    (response) => {this.message = response['message']})
}

fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    //this.product.photo = fileInput.target.files[0]['name'];
}


ngOnInit() {
    this.myStyle = {
        'position': 'absolute',
        'width': '100%',
        'height': '100%',
        'z-index': -1,
        'top': 0,
        'left': 0,
        'right': 0,
        'bottom': 0,
    };

this.myParams = {
        particles: {
            number: {
                value: 100,
            },
            color: {
                value: '#ff0000'
            },
            shape: {
                type: 'circle',
            },
            line_linked: {
              "enable": false,
              "distance": 150,
              "color": "#ffffff",
              "opacity": 0.8,
              "width": 1
            },
    }
};
}
}
